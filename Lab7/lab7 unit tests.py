import unittest
import datetime
from Lab7.lab7 import CourseTime


class TestCourseTime(unittest.TestCase):
    def setUp(self):
        self.init_one = "14:00 16:00 MWF"
        self.init_two = "15:00 17:00 MW"
        self.init_three = "13:00 15:00 TRF"
        self.init_four = "1:00 2:00 MTWRFS"
        self.init_online = "online"
        self.init_wrong = "13:00 14:00 ooo"
        self.init_wrong_too = "ooo 14:00 MW"
        self.init_wrong_aswell = "13:00 ooo MW"
        self.init_badtype = True
        self.init_badtype_too = [3, 3, "M"]

    def test_str(self):
        one = CourseTime(self.init_one)
        self.assertEqual(self.init_one, str(one), "Does not output not online str correctly.")
        online = CourseTime(self.init_online)
        self.assertEqual(self.init_online, str(online), "Does not output online str correctly.")

    def test_start(self):
        one = CourseTime(self.init_one)
        self.assertEqual(datetime.time(14, 0), one.start(), "Does not output proper start time for not online course")
        online = CourseTime(self.init_online)
        self.assertEqual(datetime.time(0, 0), online.start(), "Does not output proper start time for online course")

    def test_end(self):
        one = CourseTime(self.init_one)
        self.assertEqual(datetime.time(16, 0), one.end(), "Does not output proper end time for not online course")
        online = CourseTime(self.init_online)
        self.assertEqual(datetime.time(0, 0), online.end(), "Does not output proper end time for online course")

    def test_isOnline(self):
        one = CourseTime(self.init_one)
        self.assertFalse(one.isOnline(), "Not online course returns online status")
        online = CourseTime(self.init_online)
        self.assertFalse(online.isOnline(), "Online course returns not online status")

    def test_isOverlap(self):
        one = CourseTime(self.init_one)
        two = CourseTime(self.init_two)
        three = CourseTime(self.init_three)
        four = CourseTime(self.init_four)
        online = CourseTime(self.init_online)
        self.assertTrue(one.isOverlap(two), "end overlap failure")
        self.assertTrue(one.isOverlap(three), "beginning overlap failure")
        self.assertTrue(two.isOverlap(one), "beginning overlap failure B")
        self.assertTrue(three.isOverlap(one), "end overlap failure B")
        self.assertFalse(one.isOverlap(four), "faulty overlap detected")
        self.assertFalse(one.isOverlap(online), "online overlap not online")

    def test_bad_init_type(self):
        self.assertIs(CourseTime(self.init_one), CourseTime, "Normal string input fails init")
        self.assertRaises(TypeError, CourseTime(self.init_badtype))
        self.assertRaises(TypeError, CourseTime(self.init_badtype_too))

    def test_bad_init_value(self):
        self.assertIs(CourseTime(self.init_online), CourseTime)
        self.assertRaises(ValueError, CourseTime(self.init_wrong))
        self.assertRaises(ValueError, CourseTime(self.init_wrong_too))
        self.assertRaises(ValueError, CourseTime(self.init_wrong_aswell))


if __name__ == "__main__":
    unittest.main()
