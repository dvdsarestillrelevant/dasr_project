class CourseTime:

    # __init__(self, time) takes in a string in the format "<start time> <end time> <days>" where days can only be a
    # combination of M, T, W, R, F, or S. Alternatively "Online" may be entered for an online class.
    # Does not return. Side effects is the input is parsed into proper variables.
    def __init__(self, time):
        pass

    # start(self) returns the start time as a datetime.time object without any external args or side effects.
    # Online classes return a value of midnight.
    def start(self):
        pass

    # end(self) returns the end time as a datetime.time object without any external args or side effects.
    # Online classes return a value of midnight.
    def end(self):
        pass

    # isOnline(self) returns a boolean indicating whether the class is online or not without any external
    # args or side effects.
    def isOnline(self):
        pass

    # isOverlap(self, other) takes in another CourseTime object and returns a boolean indicating whether the
    # scheduled times for each course overlap without any other side effects.
    def isOverlap(self, other):
        pass

    # __str__(self) returns a string version of the values in this object in the same format as the __init__ argument.
    # "<start time> <end time> <days>" where days can only be a combination of M, T, W, R, F, or S.
    # Alternatively "Online" may be returned for an online class.
    def __str__(self):
        pass
