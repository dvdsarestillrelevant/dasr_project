import unittest

class AcceptanceTests:
    def setup(self):
        pass

    def test_login(self):
        '''test login info
        if username enters incorrect info, should be prompted to reenter username and password

        '''
        pass

    def test_all_specify(self):
        pass

    def test_help(self):
        pass

    def test_change_pass(self):
        pass

    # Instructor
    def test_instructor_edit(self):
        pass

    def test_course_view(self):
        pass

    def test_ta_view(self):
        pass

    def test_notify(self):
        pass

    def test_ta_assign(self):
        pass

    # TA
    def test_ta_edit(self):
        pass

    def test_schedule(self):
        pass

    # Admin
    def create_account(self):
        pass

    def test_create_course(self):
        '''
        time = [(1000-1250),(),
        cs250 = CSclass(250, "Instructor Bob", "TA Jill", time)
        self.assertEqual(cs250.name, 250)
        self.assertEqual(cs250.instructor, "Instructor Bob")
        self.assertEqual(cs250.TA.name, "TA Jill")
        self.assertEqual(cs250.time, time)
        '''
        pass

    def test_delete_account(self):
        ''' Test delete Admin account, should fail'''
        pass

    def test_delete_class(self):
        ''' Test to delete the class
        if class doesnt exist, should return "class does not exist"
        if class exists, should fail
        '''
        pass

    def test_edit_account(self):
        pass

    def test_edit_class(self):
        pass

    def test_assign_instructor(self):
        # Assign Instructor to a class
        pass

if __name__ == '__main__':
    unittest.main()